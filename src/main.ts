﻿import { Aurelia } from 'aurelia-framework';
// we want font-awesome to load as soon as possible to show the fa-spinner
import '../styles/styles.css';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

// comment out if you don't want a Promise polyfill (remove also from webpack.config.js)
import * as Bluebird from 'bluebird';
Bluebird.config({ warnings: false });

const myConfiguration = {
  'aurelia-form': {
    // The default element used, probably best left unchanged
    defaultElement: 'input',

    // The default behavior to use for forms
    defaultBehavior: 'regular',

    // Register your (custom) elements here
    elements: {
      // Example
      'custom-longtext': 'custom-longtext'
    },

    // Configured by aurelia-form-validation
    validation: {},

    // Submit button configuration for <aurelia-form /> and <entity-form />
    submitButton: {
      enabled: true,        // Show the button
      options: ['primary'], // Options to pass to the button
      label: 'Submit'     // Default (fallback) label of the button
    },

    // Alias these entity types to elements (used by aurelia-orm)
    aliases: {
      enum: 'radio',
      int: 'input',
      integer: 'input',
      number: 'input',
      float: 'input',
      string: 'input',
      bool: 'checkbox',
      boolean: 'checkbox',
      text: 'textarea'
    }
  }
};

export async function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .feature('resources')
    .plugin('aurelia-validation')
    .plugin('aurelia-view-manager')
    .plugin('aurelia-config', config => {
      return config([
        'aurelia-form'
        // Other plugins
      ], myConfiguration);
    })
    .plugin('aurelia-form-validation');

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin('aurelia-animator-css');
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin('aurelia-html-import-template-loader')

  await aurelia.start();
  aurelia.setRoot('app');

  // if you would like your website to work offline (Service Worker), 
  // install and enable the @easy-webpack/config-offline package in webpack.config.js and uncomment the following code:
  /*
  const offline = await System.import('offline-plugin/runtime');
  offline.install();
  */
}